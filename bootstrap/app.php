<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;


require_once __DIR__.'/../vendor/autoload.php';

$env = require_once __DIR__.'/../env.php';

foreach ($env as $envi) {
    putenv($envi);
}

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/../app"), $isDevMode);
// database configuration parameters
$conn = array(
    'driver' => 'pdo_sqlite',
    'path' => __DIR__ . '/../database/db.sqlite',
);
// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);
