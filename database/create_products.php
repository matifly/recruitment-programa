<?php

use App\Product;

require_once __DIR__."/../bootstrap/app.php";

$products = [
    [
        'name' => 'produkt 1',
        'description' => 'opis produktu 1',
        'price' => 100,
        'available' => true,
    ],  [
        'name' => 'produkt 2',
        'description' => 'opis produktu 2',
        'price' => 150,
        'available' => true,
    ], [
        'name' => 'produkt 3',
        'description' => 'opis produktu 3',
        'price' => 200,
        'available' => false,
    ], [
        'name' => 'produkt 4',
        'description' => 'opis produktu 3',
        'price' => 250,
        'available' => false,
    ],
];

foreach($products as $productData) {
    $product = new Product();
    $product->setName($productData['name']);
    $product->setDescription($productData['description']);
    $product->setPrice($productData['price']);
    $product->setAvailable($productData['available']);
    $entityManager->persist($product);
}
$entityManager->flush();
