<?php

require_once __DIR__."/../bootstrap/app.php";

$categories = [
    [
        'name' => 'Kategoria 1',
        'products' => [1,2,3,4],
    ], [
        'name' => 'Kategoria 2',
        'products' => [1,2]
    ]
];

foreach($categories as $categoryData) {
    $category = new \App\Category();
    $category->setName($categoryData['name']);
    foreach ($categoryData['products'] as $productId) {
        $product = $entityManager->find(\App\Product::class, $productId);
        $category->assignToProduct($product);
    }
    $entityManager->persist($category);
}
$entityManager->flush();
