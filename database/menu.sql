DROP TABLE IF EXISTS `menu`;
CREATE TABLE menu (
  id        int unsigned auto_increment primary key,
  name      VARCHAR(255) NOT NULL,
  parent_id int unsigned null,
  FOREIGN KEY (parent_id) REFERENCES menu (id)
);
insert into menu (name, parent_id)
values ('Kategoria główna', null);
insert into menu (name, parent_id)
values ('Podkategoria 1', 1);
insert into menu (name, parent_id)
values ('Podkategoria 2', 1);
insert into menu (name, parent_id)
values ('Podkategoria 1.1', 2);
insert into menu (name, parent_id)
values ('Podkategoria 1.2', 2);
insert into menu (name, parent_id)
values ('Podkategoria 2.1', 3);
insert into menu (name, parent_id)
values ('Podkategoria 2.2', 3);





