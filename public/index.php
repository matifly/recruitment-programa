<?php
require_once '../bootstrap/app.php';
$menu = new \App\MenuRepository();
$menu = $menu->getMenu();
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="UTF-8">
    <title>Menu</title>
    <link rel="stylesheet" type="text/css" href="../resources/styles/main.css">
    <script type="module" src="../resources/js/form-validation.js"></script>
</head>
<body>
<div id="menu" class="menu">



    <div class="navbar">
        <?php
        foreach ($menu as $menuItem) {
            echo  '<div class="dropdown">';
            echo ' <button class="dropbtn">'.$menuItem['name'].'</button>';
            echo '<div class="dropdown-content">';
            foreach ($menuItem["children"] as $submenu) {
                echo '<div class="first">';
                echo '<a>'.$submenu["name"].'</a>';
                echo '<div class="second">';
                foreach($submenu["children"] as $secondLevelItem) {
                    echo '<a>'.$secondLevelItem['name'].'</a>';
                }
                echo '</div>';
                echo '</div>';
            }
            echo '</div>';
            echo '</div>';
        }
        ?>

    </div>

</div>
<div class="content">

    <form name="form" method="POST">
            <span class="formField textField">
                <label for="name">Imię: </label>
                <input id="name" name="name" placeholder="Imię" type="text">
            </span>
        <span class="formField textField">
            <label for="surname">Nazwisko: </label>
            <input id="surname" type="text" name="surname" placeholder="Nazwisko">
            </span>

        <span class="formField textField">
            <label for="age">Wiek: </label>
            <input id="age" name="age" type="number" step="1">
            </span>

        <span class="formField">
            Płeć <br>
            <label for="male">Mężczyzna
                <input id="male" name="gender" value="0" type="radio">
            </label>
            <label for="female">Kobieta
                <input id="female" name="gender" value="1" type="radio">
            </label>
        </span>

        <span class="formField">
        <input id="terms" type="checkbox"> Wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w mojej
        ofercie pracy dla potrzeb niezbędnych do realizacji procesu rekrutacji (zgodnie z Ustawą z dnia 29.08.1997 roku
        o Ochronie Danych Osobowych; tekst jednolity: Dz. U. z 2002r. Nr 101, poz. 926 ze zm.).
       </span>

        <button type="submit">Zatwierdź formularz</button>


    </form>


    <span class="cv">
            <img src=".\..\resources\files\cv.jpg">
        </span>

</div>


</body>
</html>