<?php

namespace Testing;

use App\Product;
use App\ProductRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class ProductRepositoryTest extends TestCase
{
    /** @var EntityManager */
    private $entityManager;
    /** @var ProductRepository */
    private $productRepository;

    protected function setUp(): void {
        parent::setUp();
        $this->entityManager = $GLOBALS['entityManager'];
        $this->productRepository = $this->entityManager->getRepository(Product::class);
    }

    public function testCountAvailable() {
        $count =  $this->productRepository->countAvailable();
        $this->assertTrue(is_int($count));
    }

    public function testGetUnavailable() {
        $result =  $this->productRepository->getUnavailable();
        $this->assertInstanceOf(Collection::class, $result);
    }

    public function testFindByPhrase() {
        /** @var Collection $result */
        $result =  $this->productRepository->findByPhrase('4');
        $this->assertTrue($result->first()->getName() =="produkt 4");
    }

}
