<?php

namespace Testing;

use App\Category;
use App\CategoryRepository;
use App\Product;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class CategoryRepositoryTest extends TestCase
{
    /** @var EntityManager */
    private $entityManager;
    /** @var CategoryRepository */
    private $categoryRepository;

    protected function setUp(): void {
        parent::setUp();
        $this->entityManager = $GLOBALS['entityManager'];
        $this->categoryRepository =
            $this->entityManager->getRepository(Category::class);
    }

    public function testGetAvailableProductByCategory() {
        /** @var Product[] $products */
        $products = $this->categoryRepository->getAvailableProductsByCategory(2);
        foreach ($products as  $product) {
            $this->assertTrue($product->isAvailable());
        }
    }

    public function testGetProductsByCategorySorted() {
        /** @var Product[] $products */
        $products = $this->categoryRepository->getGetProductByCategorySortedByName(1, 'DESC');
        $this->assertEquals($products[0]->getName(), 'produkt 4');
    }
}
