import {Validator} from "./Validator";

document.forms["form"].addEventListener("submit", function () {
    event.preventDefault();
    validateForm();
});

const validationRules = {
    "name": {
        rules: ["required"],
        error: "Imię jest wymagane.",
    },
    "surname": {
        rules: ["required"],
        error: "Nazwisko jest wymagane.",
    },
    "age": {
        rules: ["required", "between:18:99"],
        error: "Wiek jest wymagany i musi mieścić się w przedziale 18-99.",
    },
    "gender": {
        rules: ["required"],
        error: "Płeć jest wymagana.",
    },
    "terms": {
        rules: ["checkbox"],
        error: "Musisz zatwierdzić zgodę na przetwarzanie danych osobowych.",
    },


};


function validateForm() {
    try {
        for (let key in validationRules) {
            validationRules[key]["rules"].forEach((rule) => validateFieldByRule(rule, key));
        }
    } catch (e) {
        if (typeof e === "string")
            alert(e)
    }
    return true;
}

function validateFieldByRule(rule, key) {
    let field = document.form[key];
    if (rule === "checkbox") {
        if(field.checked)
            return true
        throw validationRules[key].error;
    }

    let ruleArray = rule.split(":");
    let [ruleName, ...ruleParams] = ruleArray;
    if (!Validator.validate(ruleName, field.value, ruleParams)) {
        throw validationRules[key].error
    }
    return true;

}






