export class Validator {

    static validate(rule, value, params) {
        let fn = this[rule];
        if(typeof fn === 'function') {
            return fn(value, ...params);
        }
    }

    static required(value) {
        return !(value === "" || value === null || value === undefined)
    }

    static max(value, max=0) {
        return value <= max;
    }

    static min(value, min=0) {
        return value <= min;
    }

    static between(value, min = 0, max = 100) {
        return !(value < min || value > max);
    }

}