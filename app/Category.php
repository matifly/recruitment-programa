<?php

namespace App;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity(repositoryClass="CategoryRepository")
 * @Table(name="categories")
 **/
class Category
{
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @var int
     * @Id @Column(type="integer") @GeneratedValue
     */
    protected $id;
    /**
     * @var string
     * @Column(type="string")
     */
    protected $name;

    /**
     * @ManyToMany(targetEntity="Product"),
     * @JoinTable(name="category_product")
     **/
    protected $products = null;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void {
        $this->name = $name;
    }

    /**
     * @param Product $product
     */
    public function assignToProduct(Product $product)
    {
        $this->products[] = $product;
    }


    public function getProducts()
    {
        return $this->products;
    }


}