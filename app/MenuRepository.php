<?php

namespace App;

use mysqli;

class MenuRepository
{
    private $db;

    public function getMenu() {
        $this->connect();
        $result = $this->db->query("SELECT * from menu where parent_id is null");
        $mainItems = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $this->getChildrenRecursively($mainItems);
        return $mainItems;
    }

    private function connect() {
        $this->db = new mysqli(getenv("DB_HOST"), getenv("DB_USER"), getenv("DB_PASSWORD"), getenv("DB_DATABASE")) or die("SQL connection error");
        $this->db->set_charset("utf8");
    }
    
    private function getChildrenRecursively(&$array) {
        foreach ($array as &$element) {
            $result = $this->db ->query("SELECT * from menu where parent_id={$element['id']}");
            $subElementsArray = mysqli_fetch_all($result,MYSQLI_ASSOC);
            $element["children"] = $subElementsArray;
            $this->getChildrenRecursively($element["children"]);
        }
    }

}