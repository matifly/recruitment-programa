<?php

namespace App;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function getAvailableProductsByCategory(int $categoryId) {
        $repository = $this->_em->getRepository(Product::class);
        $result = $repository->createQueryBuilder('p')
            ->innerJoin('p.categories', 'c')
            ->where('c.id = :categoryId')
            ->andWhere('p.available = true')
            ->setParameter('categoryId', $categoryId)
            ->getQuery()
            ->getResult();
        $collection = new ArrayCollection($result);
        return $collection;
    }

    public function getGetProductByCategorySortedByName(int $categoryId, $order = "ASC") {
        $repository = $this->_em->getRepository(Product::class);
        $result = $repository->createQueryBuilder('p')
            ->innerJoin('p.categories', 'c')
            ->where('c.id = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->orderBy('p.name', $order)
            ->getQuery()
            ->getResult();
        $collection = new ArrayCollection($result);
        return $collection;
    }
}