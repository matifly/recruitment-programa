<?php

namespace App;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{

    public function countAvailable() : int {
        $result = $this->_em->createQuery('select count(p) from App\Product p where p.available = true')->getSingleScalarResult();
        return (int) $result;
    }

    public function getUnavailable() : Collection {
        $result = $this->_em->createQueryBuilder()->select('p')
            ->from(Product::class, 'p')
            ->where('p.available = false')
            ->getQuery()->getResult();

        $collection = new ArrayCollection($result);
        return $collection;
    }

    public function findByPhrase(string $phrase) {

        $result = $this->_em->createQueryBuilder()->select('p')
            ->from(Product::class, 'p')
            ->where("p.name like '%{$phrase}%'")
            ->getQuery()
            ->getResult();

        $collection = new ArrayCollection($result);
        return $collection;
    }


}