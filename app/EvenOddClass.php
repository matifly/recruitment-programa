<?php

namespace App;

class EvenOddClass
{
    /** @var array */
    private $numbers = [];

    public function addNumber(int $number) {
        if ($number > 0)
            array_push($this->numbers, $number);
    }

    public function getNumbers() {
        return $this->numbers;
    }

    public function tripleEvenOrDobuleOdd() {
        $countEven = $this->countEven();
        $countOdd = $this->countOdd();

        switch (true) {
            case $countEven * 3 > $countOdd * 2 :
                return 'even';
            case $countEven * 3 < $countOdd * 2 :
                return 'odd';
            default :
                return 'equal';
        }

    }

    private function countOdd() {
        $counter = 0;
        foreach ($this->numbers as $number) {
            if ($number % 2 != 0) {
                $counter++;
            }
        }
        return $counter;
    }

    private function countEven() {
        $counter = 0;
        foreach ($this->numbers as $number) {
            if ($number % 2 == 0) {
                $counter++;
            }
        }
        return $counter;
    }
}