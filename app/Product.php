<?php

namespace App;

/**
 * @Entity(repositoryClass="ProductRepository")
 * @Table(name="products")
 **/
class Product
{
    /**
     * @var int
     * @Id @Column(type="integer") @GeneratedValue
     */
    protected $id;
    /**
     * @var string
     * @Column(type="string")
     */
    protected $name;
    /**
     * @var string
     * @Column(type="string")
     */
    protected $description;
    /**
     * @var int
     * @Column(type="integer")
     */
    protected $price;
    /**
     * @var bool
     * @Column(type="boolean")
     */
    protected $available;
    /**
     * @ManyToMany(targetEntity="Category"),
     * @JoinTable(name="category_product")
     **/
    protected $categories = null;

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isAvailable(): bool {
        return $this->available;
    }

    /**
     * @param bool $available
     */
    public function setAvailable(bool $available): void {
        $this->available = $available;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void {
        $this->description = $description;
    }

    /**
     * @param Category $category
     */
    public function assignToCategory(Category $category)
    {
        $this->categories[] = $category;
    }


    public function getCategories()
    {
        return $this->categories;
    }

}